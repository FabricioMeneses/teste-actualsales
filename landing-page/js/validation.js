$(function($) {
    var nome, dtNasc, telefone, email, regiao, unidade;
    nome = '';
    dtNasc = '';
    telefone = '';
    email = '';

    // Máscaras
    $('#telefone').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/D/g, '');
        if(phone.length > 14) {
            element.mask('(99) 99999-999?9');
        } else {
            element.mask('(99) 9999-9999?9');
        }
    }).trigger('focusout');

    // Regras de Validação
    $('.next-step1').click(function (event) {
        $('#step_1').validate({
            rules: {
                'nome': {
                    required: true
                },
                'data_nascimento': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'telefone': {
                    required: true
                }
            },
            messages: {
                'nome': {
                    required: 'O nome é obrigatório.',
                },
                'data_nascimento': {
                    required: 'A data de nascimento é obrigatória.',
                    date: 'Informe uma data válida.'
                },
                'email': {
                    required: 'O email é obrigatório.',
                    email: 'Informe um email válido.'
                },
                'telefone': {
                    required: 'O telefone é obrigatório.'
                }
            }
        });

        if ($('#step_1').valid()) {
            nome = $('#nome').val();
            dtNasc = $('#data_nascimento').val();
            telefone = $('#telefone').val();
            email = $('#email').val();
            event.preventDefault();  
            $(this).parents('.form-step').hide().next().show();
        }
    });

    $('.next-step2').click(function (event) {
        $('#step_2').validate({
            rules: {
                'regiao': {
                    required: true
                },
                'unidade': {
                    required: true
                }
            },
            messages: {
                'regiao': {
                    required : 'Selecione uma região.'
                },
                'unidade': {
                    required: 'Selecione uma unidade.'
                }
            }
        });
        if ($('#step_2').valid()) {
            regiao = $('#regiao').val();
            unidade = $('#unidade').val();
            event.preventDefault();  
            $(this).parents('.form-step').hide().next().show();
            console.log(regiao, unidade);
        }
    });

});
